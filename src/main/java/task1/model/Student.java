package task1.model;

public class Student {
    public Integer id;
    public String fio;
    public String city;
    public Group group;

    public Student(Integer id, String fio, String city, Group group) {
        this.id = id;
        this.fio = fio;
        this.city = city;
        this.group = group;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null) {return false;}
        return (
                (((Student)obj).id.equals(this.id))&&
                        ((Student)obj).city.equals(this.city)&&
                                (((Student)obj).fio.equals(this.fio))&&
                                (((Student)obj).group.equals(this.group))
               );
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", fio='" + fio + '\'' +
                ", city='" + city + '\'' +
                ", group=" + group +
                '}';
    }
}
