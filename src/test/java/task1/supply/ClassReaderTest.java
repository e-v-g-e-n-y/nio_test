package task1.supply;


import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class ClassReaderTest {

    @Test
    public void FindFileInNull(){
        Path path = new ClassReader().FindFile(null);
        assertNull(path);
    }

    @Test
    public void FindFileInEmptyStr(){
        Path path = new ClassReader().FindFile("");
        assertNull(path);
    }

    @Test
    public void FindFileIntegro(){
        // ToDo положить файл "student9999.sv" по пути
        ClassReader classReader = new ClassReader();
        classReader.setDataFolder(StudentsFileUtils.TEST_DIRECTORY);
        Path pathResult = classReader.FindFile(StudentsFileUtils.BuildFileName(9999));
        Path pathSample = Paths.get(StudentsFileUtils.TEST_DIRECTORY + StudentsFileUtils.getSeparator()
                + "group8888" + StudentsFileUtils.getSeparator() + StudentsFileUtils.BuildFileName(9999));
        assertEquals(pathSample, pathResult);
    }

    @Test
    public void FindFileErrFolder(){
        // задан несуществующий каталог для поиска файла
        ClassReader classReader = new ClassReader();
        classReader.setDataFolder(StudentsFileUtils.TEST_DIRECTORY + "\\gyreygerjgejh");
        Path pathResult = classReader.FindFile(StudentsFileUtils.BuildFileName(9999));
        assertNull(pathResult);
    }

    @Test
    public void GetFilesOk(){
        // получение списка файлов в директории - Ok
        ClassReader classReader = new ClassReader();
        List<File> sampleFiles = new ArrayList<>();
        sampleFiles.add(new File(StudentsFileUtils.TEST_DIRECTORY + "\\333\\student1.txt"));
        sampleFiles.add(new File(StudentsFileUtils.TEST_DIRECTORY + "\\333\\student10.txt"));
        sampleFiles.add(new File(StudentsFileUtils.TEST_DIRECTORY + "\\333\\student11.txt"));
        sampleFiles.add(new File(StudentsFileUtils.TEST_DIRECTORY + "\\333\\student2.txt"));
        sampleFiles.add(new File(StudentsFileUtils.TEST_DIRECTORY + "\\333\\student3.txt"));
        List<File> findFiles = classReader.getFiles(StudentsFileUtils.TEST_DIRECTORY + "\\333");
        assertEquals(sampleFiles, findFiles);
    }

    @Test
    public void ReadWriteFileIntegro() throws IOException {
        // ToDo положить файл "student9999.txt" по пути
        String FILE_CONTENT = "This is a test string for read/write test...";
        String strFolder = StudentsFileUtils.TEST_DIRECTORY + "\\555";
        String fileName = strFolder + "\\readwrite.test";
        Path path = Paths.get(fileName);
        if (Files.exists(path)) {
            File file = new File(fileName);
            file.delete();
        }

        ClassReader classReader = new ClassReader();
//        classReader.setDataFolder(StudentsFileUtils.TEST_DIRECTORY);
        classReader.SaveFile(FILE_CONTENT, fileName);
        String resString = classReader.LoadFile(fileName);
        assertEquals(FILE_CONTENT, resString);
    }

}
