package task1.model;

public class Group {
    public Integer id;
    public Integer graduationYear;

    public Group(Integer id, Integer graduationYear) {
        this.id = id;
        this.graduationYear = graduationYear;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {return false;}
        return (
                (((Group)obj).graduationYear.equals(this.graduationYear))&&
                        (((Group)obj).id.equals(this.id))
        );
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", graduationYear=" + graduationYear +
                '}';
    }
}
