package task1.supply;

public class StudentsFileUtils {
    public static final String FILE_DIRECTORY = "D:\\JAVA_DEBUG\\students\\data";
    public static final String TEST_DIRECTORY = "D:\\JAVA_DEBUG\\students\\tests";

    public static final String STUDENTS_FILE_PREFIX = "student";
    public static final String STUDENTS_FILE_EXTENSION = ".sv";
    public static final String GROUP_FOLDER_PREFIX = "group";

    public static String getSeparator() {
        return "\\";
    }

    public static String getGroupFolder(Integer idGroup) {
        return FILE_DIRECTORY + StudentsFileUtils.getSeparator() + GROUP_FOLDER_PREFIX + idGroup;
    }

    public static String BuildFileName(Integer id) {
        //
        String filename = STUDENTS_FILE_PREFIX + id + STUDENTS_FILE_EXTENSION;
        return filename;
    }
    public static String BuildAbsoluteFilename(Integer idStudent, Integer idGroup) {
        String filename = getGroupFolder(idGroup)
                + StudentsFileUtils.getSeparator() + BuildFileName(idStudent);
        return filename;
    }

}
