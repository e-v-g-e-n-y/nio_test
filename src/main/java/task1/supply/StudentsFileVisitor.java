package task1.supply;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;


public class StudentsFileVisitor extends SimpleFileVisitor<Path> {
    private String filename = null;
    private Path foundFile = null;


    public Path getFoundFile() {
        return foundFile;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        boolean confirmedName = true;
        if (filename != null && !file.getFileName().toString().equals(filename))
            confirmedName = false;

        if (confirmedName) {
            foundFile = file.toAbsolutePath();
            return FileVisitResult.TERMINATE;
        }
        else {
            return FileVisitResult.CONTINUE;
        }
    }

    public void setNameToSearch(String nameTemplate) {
        this.filename = nameTemplate;
    }
}
