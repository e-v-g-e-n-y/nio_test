package task1.service;

import task1.supply.ClassReader;
import task1.supply.StudentToJsonClass;
import task1.model.Student;
import task1.supply.StudentsFileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class Service {
    private ClassReader classReader;

    public Service(ClassReader classReader) {
        this.classReader = classReader;
    }
    public boolean Save(Student student) {
        try {
            if (student == null) {
                throw new Exception("Получен на вход пустой объект <Student>");
            }
            if (student.group == null) {
                throw new Exception("Получен на вход пустой объект <Group>");
            }

            String filename = StudentsFileUtils.BuildAbsoluteFilename(student.id, student.group.id);
            String jsonStr = StudentToJsonClass.StudentToJson(student);
            classReader.SaveFile(jsonStr, filename);
            return true;
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
            return false;
        }
    }
    public Student Load(String fileName) {
        String jsonText = null;
        try {
            jsonText = classReader.LoadFile(fileName);
        } catch (IOException e) {
            e.printStackTrace();
            return  null;
        }
        Student student = StudentToJsonClass.JsonToStudent(jsonText);
        return student;
    }
    public Student Get(Integer id){
        try {
            // формирование имени файла, который будем искать по каталогам
            String filename = StudentsFileUtils.BuildFileName(id);
            // поиск, получение полного пути к файлу
            Path filePath = classReader.FindFile(filename);
            if (filePath == null) {
                System.err.println("Не удалось найти файл '" + filename + "'");
                return null;
            }
            String fileAbsName = filePath.toAbsolutePath().toString();
            // рагрузка и разбор файла
            return Load(fileAbsName);
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
            return null;
        }
    }
    public List<Student> getStudents(Integer groupId) {
        // формирование пути к директории группы
        String folderPath = StudentsFileUtils.getGroupFolder(groupId);

        // получение списка файлов в указаноой дирректории
        List<File> fileList = classReader.getFiles(folderPath);

        // поочерёдная загрузка студентов
        try {
            List<Student> lst = new ArrayList<>();
            for (File item : fileList
            ) {
                Student currStudent = Load(item.getAbsolutePath());
                lst.add(currStudent);
            }
            return lst;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
