package task1.supply;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class ClassReader {
    private String dataFolder = StudentsFileUtils.FILE_DIRECTORY;
    public void SaveFile(String stringJson, String filename) throws IOException {
        //
        try (RandomAccessFile aFile = new RandomAccessFile(filename, "rw")) {
            FileChannel channel = aFile.getChannel();
            ByteBuffer buffer = ByteBuffer.wrap(stringJson.getBytes());
            channel.write(buffer);
        }
    }
    public String LoadFile(String filename) throws IOException {
        //
        try (RandomAccessFile aFile = new RandomAccessFile(filename, "rw")) {
            FileChannel channel = aFile.getChannel();
            StringBuilder stringBuilder = new StringBuilder();
            ByteBuffer buffer = ByteBuffer.allocate(48);
            int bytesRead = channel.read(buffer);
            while (bytesRead != -1) {
                buffer.flip();
                while (buffer.hasRemaining()) {
                    stringBuilder.append((char) buffer.get());
                }
                buffer.clear();
                bytesRead = channel.read(buffer);
            }
        return  stringBuilder.toString();
        }
    }
    public List<File> getFiles(String folderPath) {
        //
        File dir = new File(folderPath);
        File[] arrFiles = dir.listFiles();
        List<File> lst = Arrays.asList(arrFiles);
        return lst;
    }
    public Path FindFile(String fileName) {
        if (fileName == null) {return null;}
        if (fileName.isEmpty()) {return null;}
        try {
            FileVisitor fileVisitor = new StudentsFileVisitor();
            ((StudentsFileVisitor) fileVisitor).setNameToSearch(fileName);
            Files.walkFileTree(Paths.get(dataFolder), new HashSet<>(),2, fileVisitor);
            return ((StudentsFileVisitor) fileVisitor).getFoundFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void setDataFolder(String dataFolder) {
        this.dataFolder = dataFolder;
    }
}
