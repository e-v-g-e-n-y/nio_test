package task1.supply;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import task1.model.Student;

public class StudentToJsonClass {
 public static String StudentToJson(Student student){
     GsonBuilder builder = new GsonBuilder();
     Gson gson = builder.create();
     return  gson.toJson(student);
 }
 public static Student JsonToStudent(String jsonText) {
     if ((jsonText == null) || jsonText.isEmpty()) {
         return null;
     }
     try {
         GsonBuilder builder = new GsonBuilder();
         Gson gson = builder.create();
         Student student = gson.fromJson(jsonText, Student.class);
         return student;
     } catch (Exception e) {
         e.printStackTrace();
         return null;
     }
 }
}
