package task1.supply;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class StudentsFileAttributesTest {
    //
    @Test
    public void BuildFileNameOK() {
        Integer idStudent1 = 1;
        String resFunc1 = StudentsFileUtils.BuildFileName(idStudent1);
        String resSample1 = StudentsFileUtils.STUDENTS_FILE_PREFIX + idStudent1 + StudentsFileUtils.STUDENTS_FILE_EXTENSION;
        Integer idStudent2 = 222;
        String resFunc2 = StudentsFileUtils.BuildFileName(idStudent2);
        String resSample2 = StudentsFileUtils.STUDENTS_FILE_PREFIX + idStudent2 + StudentsFileUtils.STUDENTS_FILE_EXTENSION;
        assertEquals(true, (resSample1.equals(resFunc1) && (resSample2.equals(resFunc2))));
    }
}