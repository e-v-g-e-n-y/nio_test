package task1;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import task1.model.Group;
import task1.model.Student;
import task1.service.Service;
import task1.supply.ClassReader;
import task1.supply.StudentToJsonClass;
import task1.supply.StudentsFileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class MainTest {
    @Mock private ClassReader classReader;

    Service service; // = new Service(classReader);
    Group gr1 = new Group(1, 2010);
    Student student01in = new Student(1,"Ivanov Inan","Msk", gr1);
    Student student02in = new Student(2,"Petrov Petr","Spb", gr1);

    @Before
    public void setUp() {
        initMocks(this);
        classReader = mock(ClassReader.class);
        service = new Service(classReader);
    }

    @Test
    public void cannotFindFile(){
        boolean result = service.Save(null);
        assertEquals(false, result);
    }

    @Test
    public void saveNullStudentObject(){
        boolean result = service.Save(null);
        assertEquals(false, result);
    }

    @Test
    public void SaveStudentWithNullGroup(){
        Student studentNoGroup = new Student(1,"Ivanov Inan","Msk", null);
        boolean result = service.Save(studentNoGroup);
        assertEquals(false, result);
    }

    @Test
    public void SaveStudentCheckParams() throws IOException {
        ArgumentCaptor<String> textCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> folderCaptor = ArgumentCaptor.forClass(String.class);
        boolean result = service.Save(student01in);
        verify(classReader, times(1)).SaveFile(textCaptor.capture(), folderCaptor.capture());
        assertEquals(true, result);
        List<String> capturedTxt = textCaptor.getAllValues();
        assertEquals("{\"id\":1,\"fio\":\"Ivanov Inan\",\"city\":\"Msk\",\"group\":{\"id\":1,\"graduationYear\":2010}}", capturedTxt.get(0));
        List<String> capturedFolder = folderCaptor.getAllValues();
        assertEquals( StudentsFileUtils.FILE_DIRECTORY + "\\group1\\student1.sv", capturedFolder.get(0));
    }
/*
ArgumentCaptor<Person> peopleCaptor = ArgumentCaptor.forClass(Person.class);

List<Person> capturedPeople = peopleCaptor.getAllValues();
assertEquals("John", capturedPeople.get(0).getName());
assertEquals("Jane", capturedPeople.get(1).getName());
*/

    @Test
    public void cannotLoadStudent(){
        when (service.Get(anyInt())).thenReturn(null);
        Student result = service.Get(1);
        assertNull(result);
    }

    @Test
    public void getStudentOk() throws IOException {
        when (classReader.LoadFile(anyString())).thenReturn(StudentToJsonClass.StudentToJson(student01in));
        when (classReader.FindFile(anyString())).thenReturn(Paths.get(StudentsFileUtils.BuildAbsoluteFilename(-1, -1)));

        Student result = service.Get(1);
        assertEquals(student01in, result);
    }

    @Test
    public void getStudentOtherErrors() throws IOException {
        when (classReader.FindFile(anyString())).thenThrow(new RuntimeException(""));
        when (classReader.LoadFile(anyString())).thenReturn("dfdfhfghgjkjjj");
        Student result = service.Get(1);
        assertNull(result);
    }

    @Test
    public void getStudentFileNotFound() throws IOException {
        // файл не найден
        when (classReader.LoadFile(anyString())).thenReturn(StudentToJsonClass.StudentToJson(student01in));
        when (classReader.FindFile(anyString())).thenReturn(null);

        Student result = service.Get(1);
        assertNull(result);
    }

    @Test
    public void getStudentFileReadException() throws IOException {
        // ошибка при чтении файла
        when (classReader.LoadFile(anyString())).thenThrow(new IOException("Unit test"));
        when (classReader.FindFile(anyString())).thenReturn(Paths.get(StudentsFileUtils.BuildAbsoluteFilename(-1, -1)));

        Student result = service.Get(1);
        assertNull(result);
    }

@Test
public void getStudentsOk() throws IOException {
    // тест для получения списка группы
    List<Student> studentList = new ArrayList<>();
    studentList.add(new Student(1,"Ivanov Inan","Msk", gr1));
    studentList.add(new Student(2,"Pertov Petr","Spb", gr1));
    studentList.add(new Student(3,"Sidorov Sidr","Msk", gr1));
    studentList.add(new Student(4,"Ivanov Petr","Spb", gr1));
    studentList.add(new Student(5,"Ptrov Inan","Msk", gr1));

    List<File> fileList = new ArrayList<>();
    fileList.add(new File(StudentsFileUtils.TEST_DIRECTORY + "\\111\\student1.txt"));
    fileList.add(new File(StudentsFileUtils.TEST_DIRECTORY + "\\111\\student2.txt"));
    fileList.add(new File(StudentsFileUtils.TEST_DIRECTORY + "\\111\\student3.txt"));
    fileList.add(new File(StudentsFileUtils.TEST_DIRECTORY + "\\111\\student4.txt"));
    fileList.add(new File(StudentsFileUtils.TEST_DIRECTORY + "\\111\\student5.txt"));

    when (classReader.getFiles(anyString())).thenReturn(fileList);
    when (classReader.LoadFile(anyString())).thenReturn(StudentToJsonClass.StudentToJson(studentList.get(0)))
                                            .thenReturn(StudentToJsonClass.StudentToJson(studentList.get(1)))
                                            .thenReturn(StudentToJsonClass.StudentToJson(studentList.get(2)))
                                            .thenReturn(StudentToJsonClass.StudentToJson(studentList.get(3)))
                                            .thenReturn(StudentToJsonClass.StudentToJson(studentList.get(4)));

    List<Student> result = service.getStudents(1);
    assertEquals(studentList, result);
}

    @Test
    public void getStudentsError() throws IOException {
        // тест для получения списка группы - ошибка при загрузке файла
        List<File> fileList = new ArrayList<>();
        fileList.add(new File(StudentsFileUtils.TEST_DIRECTORY + "\\111\\student1.txt"));

        when (classReader.getFiles(anyString())).thenReturn(fileList);
        when (classReader.LoadFile(anyString())).thenThrow(new RuntimeException("Unit test: expected exception!"));

        List<Student> result = service.getStudents(1);
        assertNull(result);
    }

    @Test
    public void GroupToString() {
        // для покрытия
        String sample = "Group{" +
                "id=" + gr1.id +
                ", graduationYear=" + gr1.graduationYear +
                '}';
        assertEquals(sample, gr1.toString());
    }
    @Test
    public void StudentToString() {
        // для покрытия
        String sample = "Student{" +
                "id=" + student01in.id +
                ", fio='" + student01in.fio + '\'' +
                ", city='" + student01in.city + '\'' +
                ", group=" + student01in.group +
                '}';
        assertEquals(sample, student01in.toString());
    }
}
