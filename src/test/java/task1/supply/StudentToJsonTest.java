package task1.supply;

import org.junit.Test;
import task1.model.Group;
import task1.model.Student;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class StudentToJsonTest {

    @Test
    public void JsonToStudentOK() {
        String textJson = "{\"id\":1,\"fio\":\"Ivanov Inan\",\"city\":\"Msk\",\"group\":{\"id\":1,\"graduationYear\":2010}}";
        Group gr1 = new Group(1, 2010);
        Student sampleStud = new Student(1,"Ivanov Inan","Msk", gr1);
        Student res = StudentToJsonClass.JsonToStudent(textJson);
        assertEquals(sampleStud, res);
    }

    @Test
    public void JsonToStudentNullStr() {
        Student res = StudentToJsonClass.JsonToStudent(null);
        assertNull(res);
    }

    @Test
    public void JsonToStudentEmptyStr() {
        Student res = StudentToJsonClass.JsonToStudent("");
        assertNull(res);
    }

    @Test
    public void JsonToStudentBadJS() {
        Student res = StudentToJsonClass.JsonToStudent("dfgfdhfhfjhf");
        assertNull(res);
    }

    @Test
    public void StudentToJsOk() {
        String textSampleJson = "{\"id\":1,\"fio\":\"Ivanov Inan\",\"city\":\"Msk\",\"group\":{\"id\":1,\"graduationYear\":2010}}";
        Group gr1 = new Group(1, 2010);
        Student sampleStud = new Student(1,"Ivanov Inan","Msk", gr1);
        String resStr = StudentToJsonClass.StudentToJson(sampleStud);
        assertEquals(textSampleJson, resStr);
    }

    @Test
    public void StudentToJsNull() {
        String resStr = StudentToJsonClass.StudentToJson(null);
        assertEquals("null", resStr);
    }
}
