package task1;

import task1.model.Group;
import task1.model.Student;
import task1.service.Service;
import task1.supply.ClassReader;
import task1.supply.StudentsFileUtils;

import java.io.IOException;
import java.util.List;

public class Main {


    public static void main(String[] args) throws IOException {
        Group gr1 = new Group(1, 2010);

        Student student01in = new Student(1,"Ivanov Inan","Msk", gr1);
        Student student02in = new Student(2,"Petrov Petr","Spb", gr1);
        Student student03in = new Student(3,"Botylev Evgeny","Msk", gr1);

        ClassReader classReader = new ClassReader();
        Service service = new Service(classReader);
        service.Save(student01in);
        service.Save(student02in);
        service.Save(student03in);

        Student student01out = service.Get(1);
        System.out.println(student01out.toString());

        Student student02out = service.Get(2);
        System.out.println(student02out.toString());

        Student student03out = service.Get(3);
        System.out.println(student03out.toString());

        Student student00out = service.Get(-1);
        if (student00out != null) {
            System.out.println(student00out.toString());
        }

        List<Student> students = service.getStudents(1);
        System.out.println("Состав группы <group1>:" + students.toString());
    }
}
